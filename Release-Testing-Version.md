## OpenHarmony 3.1.2.3版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第3轮测试，验收:    |
| L0L1: http  2.0协议等                                        |
| L2: 驱动能力增强相关需求                                     |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-16**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:     http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165653/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165653-hispark_pegasus.tar.gz |
| hispark_taurus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165409/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165409-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165506/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165506-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2021-12-16**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：    http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.3/20211215_143812/version-Master_Version-Ohos_sdk_3.1.2.3-20211215_143812-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：    https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.2.3/20211215_150037/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_143541/version-Master_Version-OpenHarmony_3.1.2.3-20211215_143541-hispark_taurus_L2.tar.gz |
| RK3568版本:   http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211217_181409/version-Master_Version-OpenHarmony_3.1.2.3-20211217_181409-dayu200.tar.gz |

 

**需求列表:**

| no   | issue                                                        | feture    description                                        | platform | sig                | owner                                               |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- | ------------------ | --------------------------------------------------- |
| 1    | [I4KCO7](https://gitee.com/openharmony/build/issues/I4KCO7)  | [编译构建子系统]轻量级和标准系统支持使用统一的产品配置       | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)             |
| 2    | [I4JQ2N](https://gitee.com/openharmony/communication_netstack/issues/I4JQ2N) | [电话服务子系统]提供Http JS API                              | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 3    | [I4JQ3G](https://gitee.com/openharmony/third_party_nghttp2/issues/I4JQ3G) | [电话服务子系统]提供Http 2.0协议                             | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 4    | [I4LZZF](https://gitee.com/openharmony/drivers_framework/issues/I4LZZF) | [驱动子系统]支持同步/异步电源管理调用                        | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)             |
| 5    | [I4L3LF](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3LF) | [驱动子系统]传感器驱动模型能力增强                           | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)           |
| 6    | [I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR) | [驱动子系统]SR000GGUSG:【新增特性】Display-Layer HDI接口针对L2的参考实现； Display-Gralloc HDI接口针对L2的参考实现； Display-Device HDI接口针对L2的参考实现； | 标准系统 | SIG_Driver         | [@YUJIA](https://gitee.com/JasonYuJia)              |
| 7    | [I4D9V9](https://gitee.com/openharmony/drivers_framework/issues/I4D9V9) | [驱动子系统]hid类设备适配5.10内核                            | 标准系统 | SIG_Driver         | [@huangkai71](https://gitee.com/huangkai71)         |

## OpenHarmony 3.1.2.2版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.2**             |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第2轮测试，验收:    |
| L0L1: 支持Listeneer复读机                                    |
| L2: 分布式数据服务缺失功能补齐                               |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-10**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:     http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_100544/version-Master_Version-OpenHarmony_3.1.2.2-20211208_100544-hispark_pegasus.tar.gz |
| hispark_taurus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_105824/version-Master_Version-OpenHarmony_3.1.2.2-20211208_105824-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211210_092432/version-Master_Version-OpenHarmony_3.1.2.2-20211210_092432-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2021-12-10**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：    http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_093549/version-Master_Version-Ohos_sdk_3.1.2.2-20211208_093549-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：    https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_095201/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_141157/version-Master_Version-OpenHarmony_3.1.2.2-20211208_141157-hispark_taurus_L2.tar.gz |

 

**需求列表:**

| no   | issue                                                        | feture    description                              | platform | sig                | owner                                         |
| ---- | ------------------------------------------------------------ | -------------------------------------------------- | -------- | ------------------ | --------------------------------------------- |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机                | 轻量系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode)       |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode)       |
| 3    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ) | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒   | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)       |
| 4    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK) | [驱动子系统]传感器器件驱动能力增强                 | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)     |
| 5    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ) | [USB服务子系统]轻量级和标准系统使用统一的编译流程  | 标准系统 | SIG_Driver         | [@wu-chengwen](https://gitee.com/wu-chengwen) |



## OpenHarmony 3.1.2.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.2.2              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第2轮测试，验收:|
|L0L1: 主要验收分布式数据对象需求                                           |
|L2: 主要验收编译构建和驱动子系统需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-12-10**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_105824/version-Master_Version-OpenHarmony_3.1.2.2-20211208_105824-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211210_092432/version-Master_Version-OpenHarmony_3.1.2.2-20211210_092432-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_100544/version-Master_Version-OpenHarmony_3.1.2.2-20211208_100544-hispark_pegasus.tar.gz |
| **L2转测试时间：2021-12-10**                                   |
 **L2转测试版本获取路径：**                                   |
 | hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_093549/version-Master_Version-Ohos_sdk_3.1.2.2-20211208_093549-ohos-sdk.tar.gz |
 | hi3516dv300-L2版本 SDK linxu/windows：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_141157/version-Master_Version-OpenHarmony_3.1.2.2-20211208_141157-hispark_taurus_L2.tar.gz|
 | hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_095201/L2-SDK-MAC.tar.gz|

需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机   | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode)             |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐       | 标准系统 | SIG_DataManagement  | [@widecode](https://gitee.com/widecode)         |
| 3    | [I4K7E3](https://gitee.com/openharmony/build/issues/I4K7E3)  | [编译构建子系统]支持使用统一的编译命令作为编译入口      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 4    | [I4KCMM](https://gitee.com/openharmony/build/issues/I4KCMM)  | [编译构建子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 5    | [I4KCNB](https://gitee.com/openharmony/build/issues/I4KCNB)  | [编译构建子系统]支持使用统一的gn模板                   | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 6    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ)  | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒      | 标准系统    | SIG_Driver  | [@fx_zhang](https://gitee.com/fx_zhang)         |
| 7    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK)  | [驱动子系统]传感器器件驱动能力增强               | 标准系统    | SIG_Driver  | [@Kevin-Lau](https://gitee.com/Kevin-Lau)         |
| 8    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ)  | [USB服务子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_Driver  | [@wu-chengwen](https://gitee.com/wu-chengwen)         |



## OpenHarmony 3.1.1.3版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.3              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第3轮测试，验收:|
|L0L1: 不涉及                                           |
|L2: 主要验收帐号及软总线相关需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L2转测试时间：2021-11-22**                                   |
| **L2转测试版本获取路径**                                   |
|hi3516dv300-L2版本 SDK linux/windows：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_093209/version-Master_Version-OpenHarmony_3.1.1.3-20211122_093209-ohos-sdk.tar.gz
| hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.1.3/20211122_094743/L2-SDK-MAC.tar.gz
| hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_090334/version-Master_Version-OpenHarmony_3.1.1.3-20211122_090334-hispark_taurus_L2.tar.gz|

需求列表：
| no   | issue                                                        | feture description                         | platform | sig            | owner                                       |
| ---- | ------------------------------------------------------------ | ------------------------------------------ | -------- | -------------- | ------------------------------------------- |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | [软总线][传输]软总线提供传输ExtAPI接口     | 标准系统 | SIG_SoftBus    | [@laosan_ted](https://gitee.com/laosan_ted) |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | [软总线][组网]软总线支持网络切换组网       | 标准系统 | SIG_SoftBus    | [@heyingjiao](https://gitee.com/heyingjiao) |
| 3    | [I4HPR7](https://https//gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口            | 标准系统 | SIG_Driver     | [@fx_zhang](https://gitee.com/fx_zhang)     |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询           | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改 | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅     | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |


## OpenHarmony 3.1.1.3版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.3              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第3轮测试，验收:|
|L0L1: 不涉及                                          |
|L2: 主要验收帐号及软总线相关需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L2转测试时间：2021-11-22**                                   |
| **L2转测试版本获取路径**                                   |
|hi3516dv300-L2版本 SDK linux/windows：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_093209/version-Master_Version-OpenHarmony_3.1.1.3-20211122_093209-ohos-sdk.tar.gz
| hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.1.3/20211122_094743/L2-SDK-MAC.tar.gz
| hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_090334/version-Master_Version-OpenHarmony_3.1.1.3-20211122_090334-hispark_taurus_L2.tar.gz|

需求列表：
| no   | issue                                                        | feture description                         | platform | sig            | owner                                       |
| ---- | ------------------------------------------------------------ | ------------------------------------------ | -------- | -------------- | ------------------------------------------- |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | [软总线][传输]软总线提供传输ExtAPI接口     | 标准系统 | SIG_SoftBus    | [@laosan_ted](https://gitee.com/laosan_ted) |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | [软总线][组网]软总线支持网络切换组网       | 标准系统 | SIG_SoftBus    | [@heyingjiao](https://gitee.com/heyingjiao) |
| 3    | [I4HPR7](https://https//gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口            | 标准系统 | SIG_Driver     | [@fx_zhang](https://gitee.com/fx_zhang)     |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询           | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改 | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅     | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |


## OpenHarmony 3.1.1.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.2               |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第一轮测试，验收:|
|L0L1:HiStreamer相关需求，性能优化及linux版本热插拔                                            |
|L2: DFX      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-11-15**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.2/20211110_150708/version-Master_Version-OpenHarmony_3.1.1.2-20211110_150708-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.2/20211110_151619/version-Master_Version-OpenHarmony_3.1.1.2-20211110_151619-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony__3.1.1.2/20211110_151211/version-Master_Version-OpenHarmony_3.1.1.2-20211110_151211-hispark_pegasus.tar.gz |

需求列表:
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I4DK89](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK89) | 【需求】HiStreamer插件框架需求                             | 轻量系统 | SIG_GraphicsandMedia| [@guodongchen](https://gitee.com/guodongchen)  |
| 2   | [I4DK8D](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK8D) | 【需求】HiStreamer性能和DFX需求                            | 轻量系统 | SIG_GraphicsandMedia| [@guodongchen](https://gitee.com/guodongchen)  |
| 3   | [I3ND6Y](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ND6Y) | 【性能】OS内核&驱动启动优化                            | 轻量系统 | SIG_Kernel| [@kkup180](https://gitee.com/kkup180) |
| 4   | [I3NTCT](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NTCT) | Linux版本init支持热插拔                            | 轻量系统 | SIG_BscSoftSrv| [@handyohos](https://gitee.com/handyohos) |


## OpenHarmony 3.1.0.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.0.2               |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第七轮测试，验收:|
|L0L1:无                                              |
|L2: DFX      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-10-28**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_093735/version-Master_Version-OpenHarmony_3.1.0.2-20211027_093735-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094506/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094506-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094100/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094100-hispark_pegasus.tar.gz |
| **L2转测试时间：2021-10-28**                                   |
 **L2转测试版本获取路径：**                                   |
 | hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094329/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094329-hispark_taurus_L2.tar.gz |
 | hi3516dv300-L2版本 SDK linxu/windows：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211028_124231/version-Master_Version-OpenHarmony_3.1.0.2-20211028_124231-ohos-sdk.tar.gz |
 | hi3516dv300-L2版本 SDK mac：<br> https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.0.2/20211028_194502/L2-SDK-MAC.tar.gz|


需求列表:
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I3XGJH](https://gitee.com/openharmony/startup_init_lite/issues/I3XGJH) | init基础环境构建                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 2   | [I3XGKV](https://gitee.com/openharmony/startup_init_lite/issues/I3XGKV) | sytemparameter管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 3   | [I3XGLN](https://gitee.com/openharmony/startup_init_lite/issues/I3XGLN) | init 脚本管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 4   | [I3XGM3](https://gitee.com/openharmony/startup_init_lite/issues/I3XGM3) | init 服务管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 5   | [I3XGMQ](https://gitee.com/openharmony/startup_init_lite/issues/I3XGMQ) | 基础权限管理                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 6   | [I3XGN8](https://gitee.com/openharmony/startup_init_lite/issues/I3XGN8) | bootimage构建和加载                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 7   | [I3XGO7](https://gitee.com/openharmony/startup_init_lite/issues/I3XGO7) | uevent 管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 8   | [I4BX5Z](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX5Z) | 【需求】HiStreamer支持音频播放和控制             | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 9   | [I4BX8A](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX8A) | 【需求】HiStreamer支持常见音频格式mp3/wav的播放   | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 10   | [I4BX9E](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX9E) | 【需求】HiStreamer播放引擎框架需求               | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
