OpenHarmony社区版本发布计划





# OpenHarmony社区版本发布计划：

| **迭代计划** | **版本**号             | **版本构建** | **版本转测试** | **版本测试完成** |
| ------------ | ---------------------- | ------------ | -------------- | ---------------- |
| IT1（API Level：8）          | OpenHarmony 3.1.0.1   | 2021/10/20    | 2021/10/20      | 2021/11/2       |
|              | OpenHarmony 3.1.0.2   | 2021/10/27   | 2021/10/27     | 2021/11/2       |
| IT2（API Level：8）             | OpenHarmony 3.1.1.1   | 2021/11/3   | 2021/11/3     | 2021/11/9         |
|              | OpenHarmony 3.1.1.2 | 2021/11/10    | 2021/11/10      | **2021/11/16**    |
|              | OpenHarmony 3.1.1.3   | 2021/11/17    | 2021/11/17      | 2021/11/23      |
|              | OpenHarmony 3.1.1.5   | 2021/11/24   | 2021/11/24     | 2021/11/30       |
|IT3（API Level：8）       | OpenHarmony 3.1.2.1   | 2021/12/1   | 2021/12/1     | 2021/12/6         |
|              | OpenHarmony 3.1.2.2 | 2021/12/8    | 2021/12/8      | 2021/12/13    |
|              | OpenHarmony 3.1.2.3 | 2021/12/15    | 2021/12/15     | 2021/12/20    |
|              | OpenHarmony 3.1.2.5(Beta) | 2021/12/22    | 2021/12/22     | **2021/12/31**    |
|IT4（API Level：8）       | OpenHarmony 3.1.3.1   | 2022/1/4   | 2022/1/5     | 2022/1/11         |
|              | OpenHarmony 3.1.3.2 | 2022/1/11    | 2022/1/12      | 2022/1/18    |
|              | OpenHarmony 3.1.3.3 | 2022/1/18    | 2022/1/19     | 2022/1/25    |
|              | OpenHarmony 3.1.3.5 | 2022/1/25    | 2022/1/26     | **2022/2/11**    |
|IT5（API Level：8）       | OpenHarmony 3.1.5.1   | 2022/2/15   | 2022/2/16     | 2022/2/22         |
|              | OpenHarmony 3.1.5.2 | 2022/2/22    | 2022/2/23      | 2022/3/1    |
|              | OpenHarmony 3.1.5.3 | 2022/3/1    | 2022/3/2     | 2022/3/8   |
|              | OpenHarmony 3.1.5.5(Release) | 2022/3/8    | 2022/3/9     | **2022/3/15**    |

# 各版本特性交付清单：


## OpenHarmony 3.1.0.1版本特性清单：

暂无



## OpenHarmony 3.1.0.2版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I3XGJH](https://gitee.com/openharmony/startup_init_lite/issues/I3XGJH) | init基础环境构建                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 2   | [I3XGKV](https://gitee.com/openharmony/startup_init_lite/issues/I3XGKV) | sytemparameter管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 3   | [I3XGLN](https://gitee.com/openharmony/startup_init_lite/issues/I3XGLN) | init 脚本管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 4   | [I3XGM3](https://gitee.com/openharmony/startup_init_lite/issues/I3XGM3) | init 服务管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 5   | [I3XGMQ](https://gitee.com/openharmony/startup_init_lite/issues/I3XGMQ) | 基础权限管理                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 6   | [I3XGN8](https://gitee.com/openharmony/startup_init_lite/issues/I3XGN8) | bootimage构建和加载                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 7   | [I3XGO7](https://gitee.com/openharmony/startup_init_lite/issues/I3XGO7) | uevent 管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 8   | [I4BX5Z](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX5Z) | 【需求】HiStreamer支持音频播放和控制             | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 9   | [I4BX8A](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX8A) | 【需求】HiStreamer支持常见音频格式mp3/wav的播放   | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 10   | [I4BX9E](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX9E) | 【需求】HiStreamer播放引擎框架需求               | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |


## OpenHarmony 3.1.1.1版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I3XGNM](https://gitee.com/openharmony/startup_init_lite/issues/I3XGNM) | 烧写模式支持                           | 轻量系统 | SIG_BscSoftSrv           | [@xionglei6](https://gitee.com/xionglei6) |
| 2    | [I4FL3F](https://e.gitee.com/open_harmony/dashboard?issue=I4FL3F) | 3516开发板多模输入触屏和Back适配验证                          | 标准系统 | SIG_HardwareMgr           | [@hhh2](https://gitee.com/hhh2) |
| 3   | [I4DK89](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK89) | 【需求】HiStreamer插件框架需求                              | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |
| 4   | [I4DK8D](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK8D) | 【需求】HiStreamer性能和DFX需求                             | 轻量系统 | SIG_GraphicsandMedia | [@guodongchen](https://gitee.com/guodongchen) |


## OpenHarmony 3.1.1.2版本特性清单：

| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I3ND6Y](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ND6Y) | 【性能】OS内核&驱动启动优化                           | 轻量系统 | SIG_Kernel           | [@kkup180](https://gitee.com/kkup180)             |
| 2    | [I3NTCT](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NTCT) | 【启动恢复子系统】Linux版本init支持热插拔                    | 轻量系统 | SIG_BscSoftSrv       | [@handyohos](https://gitee.com/handyohos)         |

## OpenHarmony 3.1.1.3版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | [软总线][传输]软总线提供传输ExtAPI接口   | 标准系统 | SIG_SoftBus   | [@laosan_ted](https://gitee.com/laosan_ted)             |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | [软总线][组网]软总线支持网络切换组网       | 标准系统 | SIG_SoftBus  | [@heyingjiao](https://gitee.com/heyingjiao)         |
| 3    | [I4HPR7](https://https://gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口        | 标准系统 | SIG_Driver  | [@fx_zhang](https://gitee.com/fx_zhang)         |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理        | 标准系统 | SIG_BscSoftSrv  | [@verystone](https://gitee.com/verystone)         |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询        | 标准系统 | SIG_BscSoftSrv  | [@verystone](https://gitee.com/verystone)         |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改        | 标准系统 | SIG_BscSoftSrv  | [@verystone](https://gitee.com/verystone)         |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅        | 标准系统 | SIG_BscSoftSrv  | [@verystone](https://gitee.com/verystone)         |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除        | 标准系统 | SIG_BscSoftSrv  | [@verystone](https://gitee.com/verystone)         |


## OpenHarmony 3.1.1.5(Beta)版本特性清单：

暂无 
## OpenHarmony 3.1.2.1版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机   | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode)             |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐       | 标准系统 | SIG_DataManagement  | [@widecode](https://gitee.com/widecode)         |

| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4K7E3](https://gitee.com/openharmony/build/issues/I4K7E3)  | [编译构建子系统]支持使用统一的编译命令作为编译入口      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |

## OpenHarmony 3.1.2.2版本特性清单：

| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4KCMM](https://gitee.com/openharmony/build/issues/I4KCMM)  | [编译构建子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 2    | [I4KCNB](https://gitee.com/openharmony/build/issues/I4KCNB)  | [编译构建子系统]支持使用统一的gn模板                   | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 3    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ)  | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒      | 标准系统    | SIG_Driver  | [@fx_zhang](https://gitee.com/fx_zhang)         |
| 4    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK)  | [驱动子系统]传感器器件驱动能力增强               | 标准系统    | SIG_Driver  | [@Kevin-Lau](https://gitee.com/Kevin-Lau)         |
| 5    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ)  | [USB服务子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_Driver  | [@wu-chengwen](https://gitee.com/wu-chengwen)         |

## OpenHarmony 3.1.2.3版本特性清单：

| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4KCO7](https://gitee.com/openharmony/build/issues/I4KCO7)  | [编译构建子系统]轻量级和标准系统支持使用统一的产品配置  | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 2    | [I4JQ2N](https://gitee.com/openharmony/communication_netstack/issues/I4JQ2N)  | [电话服务子系统]提供Http JS API  | 轻量系统    | SIG_Telephony   | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng)           |
| 3    | [I4JQ3G](https://gitee.com/openharmony/third_party_nghttp2/issues/I4JQ3G)  | [电话服务子系统]提供Http 2.0协议  | 轻量系统    | SIG_Telephony   | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng)           |
| 4    | [I4LZZF](https://gitee.com/openharmony/drivers_framework/issues/I4LZZF)  | [驱动子系统]支持同步/异步电源管理调用      | 标准系统    | SIG_Driver  | [@fx_zhang](https://gitee.com/fx_zhang)         |
| 5    | [I4L3LF](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3LF)  | [驱动子系统]传感器驱动模型能力增强   | 标准系统    | SIG_Driver  | [@Kevin-Lau](https://gitee.com/Kevin-Lau)         |
| 6    | [I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR)  | [驱动子系统]SR000GGUSG:【新增特性】Display-Layer HDI接口针对L2的参考实现； Display-Gralloc HDI接口针对L2的参考实现； Display-Device HDI接口针对L2的参考实现；      | 标准系统    | SIG_Driver  | [@YUJIA](https://gitee.com/JasonYuJia)         |
| 7    | [I4MBTS](https://gitee.com/openharmony/drivers_framework/issues/I4MBTS)  | [驱动子系统]SR000GH16G:【增强特性】 HDF-Input设备能力丰富      | 标准系统    | SIG_Driver  | [@huangkai71](https://gitee.com/huangkai71)       |

## OpenHarmony 3.1.2.5版本特性清单：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4LUG4](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LUG4)  | [内核子系统]支持CMA复用特性  | 标准系统    | SIG-Kernel   | [@liuyu](https://gitee.com/liuyoufang)           |
| 2    | [I4LX4G](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LX4G)  | [内核子系统]支持内存占用分类查询  | 标准系统    | SIG_Kernel   | [@liuyu](https://gitee.com/liuyoufang)           |

## OpenHarmony 3.1.3.1版本特性清单：

暂无
## OpenHarmony 3.1.3.2版本特性清单：

暂无
## OpenHarmony 3.1.3.3版本特性清单：

暂无
## OpenHarmony 3.1.3.5版本特性清单：
暂无
## OpenHarmony 3.1.5.1版本特性清单：

暂无
## OpenHarmony 3.1.5.2版本特性清单：

暂无
## OpenHarmony 3.1.5.3版本特性清单：

暂无
>  


###### 以上计划由OpenHarmony社区版本发布SIG组织发布